```
 - - - - - --------------------------------------- - - - - - 
                   C I R C U M L U N A R
                 T R A N S M I S S I O N S
- - - ------------------------------------------------- - - -
```

# CIRCUMLUNAR TRANSMISSIONS

This is the working repository of Circumlunar Space's (CS)
up-and-coming zine, Circumlunar Transmissions.
The main branch contains the actual issues and content of
the zine itself, in addition to the various tooling -- or
'gubbins' -- used to build, manage, and maintain the publication.

The purpose of this repository is to coordinate a
submission-editing-publishing workflow, and also to
facilitate the distribution of the zine itself. Distributing
the zine will be as simple as cloning this repo and serving
its contents from a directory of any old gopherhole or gemini
capsule.

## SUBMISSIONS

The production of this zine is a collaborative effort made by
the residents of the colonies in circumlunar space. As such,
only sundogs of the CS colonies are eligible to submit
content to the zine at this time.

Submissions may be submitted by e-mail. Users familiar with
git version control may also submit them as patches to the
"submissions" branch of the repository. This will allow the
acting editor to collect and work with submissions in a local
copy of the repository before merging (publishing) the
finalized changes to the main branch. To be clear, after
cloning this repo, run the following command before making
any changes:

```
git checkout submissions
```

After adding a submission to the "submissions" folder, add
and commit changes in the ordinary way. Then issue the
following command to generate a patch from your changes and
send it using the local e-mail system:

```
git send-email --compose --subject="<SUBJECT>" \
    --to="<ADDRESS_OF_EDITOR>"                 \
    --from="<YOUR_CS_EMAIL_ADDRESS>" HEAD^
``` 

The acting editor of each issue changes; it is decided
by discussion in the ZINE section of the CS BBS, as well
as the deadline for submission to each issue.

If the above command fails, or if git send-email is not
available on your system, generate a patch with the
following command and send it to the editor as an e-mail
attachment:

```
git format-patch HEAD^
```

```
- - - ------------------------------------------------- - - -
                                                   2021-09-01
 - - - - - --------------------------------------- - - - - - 
``` 
