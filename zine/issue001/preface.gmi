``` 
 - - - - - --------------------------------------------- - - - - - 
                       C I R C U M L U N A R
                     T R A N S M I S S I O N S
- - - ------------------------------------------------------- - - -
Issue One                                                  May 2021
```

# Preface

Back in February, around the time of the ogre incident, Jone wondered out loud on the circumlunar.space BBS, telem, about the possibility of a CS zine. The idea was well-received by the rest of us sundogs, and for some time, talk of the zine flooded telem as we proceeded to howl at the moon in excitement. Amidst this jubilee, I somehow ended up wearing the proverbial editor pants--for this inaugural issue, at least.

Anyone who has had the pleasure of perusing the many phlogs and gemlogs here at circumlunar.space will be aware of the impressive diversity of interests, talents, and backgrounds of the sundogs residing here. It will be interesting to see how these influence the zine over time, but currently what the zine is and how it will be produced is largely still up in the air. It's really just a fun experiment, and this first issue is a kind of pilot episode.

The strategy of this first run has been to just get it out there. Rather than prematurely exhausting our energy on determining what it should be, or pidgeonholing ourselves into a niche or format without actually having produced any content, we elected to first just give it a go and see what we get. So for this first issue, we have what has been endearingly termed a "topic salad." And I believe it has turned out to be quite a nutritious one at that.

Circumlunar Transmissions will be distributed exclusively over Gopher and Gemini by whoever would like to host a copy on their own gopherhole or capsule. That is, anyone can clone the git repo of the project and serve its contents from their own smolnet space. This kind of "newstand" method of distribution solves a lot of the logistical issues of where and how to bi-host such a thing in an accessible way. In addition to Gopher and Gemini editions, we intend to provide printable formats that readers can easily print-and-bind for their own enjoyment offline or to distribute locally.

It has been a pleasure to contribute something to this wonderful community of thoughtful and creative individuals whom I respect and admire sincerely. During my relatively brief inhabitation of the Zaibatsu, I've learnt a great many things and have been inspired to create and wonder about things I would not have otherwise. It is with profound gratitude and pride for this habitat and its inhabitants, and the smolnet ecosystem at large, that I present to you this first issue of our smolzine, Circumlunar Transmissions.

~mieum
April 25, 2021
Incheon, Korea

```
- - - ------------------------------------------------------- - - -
```
=> sloum_circumlunar-mixtape-001.gmi >> [ii] The Circumlunar Mixtape
=> index.gmi Circumlunar Transmisisons - Issue 1
```
 - - - - - --------------------------------------------- - - - - - 
```
