``` 
 - - - - - --------------------------------------------- - - - - - 
                       C I R C U M L U N A R
                     T R A N S M I S S I O N S
- - - ------------------------------------------------------- - - -
Issue One                                                  May 2021
```

# Manifesto of a Granular Ideologue
by tfurrows

## Passing Thoughts of an Impractical Idealist

Our ancestors stood on the banks of the Euphrates and cast their ideas into the inexhaustible current. First they cast in words, ephemeral. Then they learned permanence; epochs brought clay tablets and papyrus, script and type, bits and clouds.

Permanence dammed the river, a logjam of ideas tossed in as if each was a consummate standard. Life-giving waters slowed, intelligence waned, the elastic mind seized and crumbled. The word manifesto dates to the fourteenth century, but the notion dates to our earliest written conclusions.

This is not the manifesto to end all manifestos. It is a message in a bottle that few will find. It is a loose model for dealing with the mountainous dam of manifestos that stop the flow of creativity, passion, and progress. At the risk of making the problem worse, I proffer: 

## From a Message in a Bottle, Sitting Atop the Dam of Manifestos

* Let your mind move freely, become a seeker
* Accept that knowledge is scattered broadly
* Find kernels of truth and germinate thoughts

* Take something from every ideology, fear nothing
* Subscribe to that which you find value in
* Mistrust labels, they carry unimaginable baggage

* Acknowledge all that came before, but accept the utility of your journey
* Never believe that you have the best vision for the world
* Don't try to fashion the world to your ideals; your ideals are cursory

* Be a maximal minimalist, distill purity from immensity
* Find, purify, share, repeat
* To conclude means to cease to grow

<tfurrows@circumlunar.space>
<tfurrows@sdf.org>

```
- - - ------------------------------------------------------- - - -
```
=> ask-jone-001.gmi << [iii] Ask Jone!
=> solderpunk_whats-the-deal-with-leapseconds.gmi >> [II] What's the Deal with Leap Seconds?
=> index.gmi Circumlunar Transmisisons - Issue 1
```
 - - - - - --------------------------------------------- - - - - - 
```
