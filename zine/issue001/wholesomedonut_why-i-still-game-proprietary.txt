 - - - - - --------------------------------------------- - - - - - 
                       C I R C U M L U N A R
                     T R A N S M I S S I O N S
- - - ------------------------------------------------------- - - -
Issue One                                                  May 2021

==================[ WHY I STILL GAME PROPRIETARY ]=================
                          by wholesomedonut
                         

PROPRIETARY GAMING ISN'T ALL BAD

Since getting into the FOSS community, I see a lot of pushback
towards the gaming industry as a whole. I can see why: DRM runs
rampant, terrible business practices regularly conflagrate internet
forums, anti-cheat programs are basically consensual (and mandatory
for official online play in some cases) trojans, and to make it all
worse it costs a mint to get into the hobby nowadays due to
scalpers and crypto miners running rampant in the market.

I agree with all of those observations. They tire me. They concern
me. They frustrate me daily.

However! There are still reasons -not- to go the route of some I
see in the FOSS world and eschew gaming altogether on anything but
a FOSS platform, with FOSS games, because.... FOSS. That argument
is just as dumb in practice, because it's an artificial limitation
that stands on somewhat subjective, opinionated reasoning. "But
wholesomedonut, thou angereth me!" I hear in the imaginary comments
section because this is Gemini and you can't do that. I am certain
you will find peace through measured contemplation and a cup of
whatever warm or cold liquid you enjoy.


WHY DO I USE STEAM?

Well.... everyone else that isn't a computer nerd usually does too.
And the overhead for getting people of minimum technical
understanding (that like playing video games) into FOSS gaming
generally is much more costly in terms of mental and social
capacity than the clout I usually have with my friends or family on
such matters.

King's English: If I have to instruct them to download the latest
version of the game directly from Github in the Releases tab, or
from some random website they've never heard of (even if it looks
nice and is HTTPS secure) instead of just adding it on Steam or
Epic or Microsoft Store or PS/Xbox or some-other thing, there is a
solid 90% chance I'm going to lose that argument unless they are
very specifically interested in that particular kind of game, its'
content, or have a better socially-driven reason. This comes from
years of trying and f'nagling with people from many walks of life;
the UI and UX of FOSS gaming needs to be on-par with modern
commercial offerings; this means all the way from landing on a
page, to funneling through a sales conversion or free download, to
playing the game with their friends needs to be understandable,
unobtrusive and transparent. That is, if the overall userbase is to
grow and sustain itself on a higher magnitude than current.


GIVE PEOPLE THE BENEFIT OF THE DOUBT

People are intelligent, generally. They're very skilled in a
multitude of things that aren't computers. But asking someone who
-isn't- tech savvy to figure out how to pull down the right version
of a FOSS game from a code repo (or heaven forbid build it
themselves with cmake or whatever) is like asking ME to diagnose a
car's problems using nothing but a flashlight and a screwdriver. I
have no idea what the hell I'm doing anyway in that department.
Without the proper tools and education too? I'm screwed. Therefore
I urge empathy and patience in introducing others to FOSS gaming.
It's a bit more finicky than the plug-and-play mentality commercial
systems have fostered.


ENTER THE MECH MAN

A good example of a FOSS game that has plenty of good and bad would
be MegaMek. It's basically a fully computerized version of the
Classic Battletech rules, which is a board game that's existed
since the 1980s and is going on 40 years of conniving,
number-crunching tomfoolery that only a particular subset of people
even enjoy. All in the name of combined-arms strategy on a hex
board that involves groups of multi-ton robots, tanks, airplanes
and infantry taking and giving damage to individual components,
weapons and armor locations in a somewhat realistic and highly
detailed simulation of 31st-century warfare.

To alleviate the issue of significant calculational overhead for
every-single-action-attack-or-damage-roll-ever, this program does
all of the math, calculations, and rules proofing for you. So you
can enjoy the game with others, wherever they may be, instead of
reaching for your G.A.T.O.R. card for the tenth time to show the
newbie of the group what happens when an SRM-6 missile spread hits
a light vehicle whose armor is already exposed on its' left flank.
There are plenty of grognards out there who know these rules well
and can do half the game in their head: they're obviously not the
target of this article.

I shiver at the thought of doing all that stuff manually if I don't
have to. That kind of tedium takes away from the moment-by-moment
gameplay, forcing everybody to get ox-in-the-mired over details
that don't matter overall instead of letting their big stompy
robots blow each other up.

Megamek works wonders in that regard. A game of Classic Battletech
that could easily take 5 or 6 hours in person without any sort of
calculator apps or an otherwise breakneck pace of gameplay and
rules-lawyering will only take an hour or two maximum with Megamek.
It's a godsend for a hobby that would otherwise be relegated to
local play over predetermined days, not a "Hey want to play a
match? Sure!" kind of casual pickup on a boring afternoon.


BUT IT ISN'T ALL SUNSHINE AND RAINBOWS

Nope! Megamek is, in my humble and donut-shaped opinion, a terrible
example of UI and UX. I played a round recently, and another
aficionado of the series played against me. Quoth my opponent:
"This program looks like something out of Windows 95." Neither of
us had played the most recent version of the game. I hadn't touched
it in a year at least.

Some changes were welcome, and the development progresses smoothly.
But it's still just as much a spaghetti plate in terms of user
experience: configuration options laid out in long lists of check
boxes organized by multiple top-window tabs; a decidedly
15-years-old design language that clashes with modern perceptions
of UI and UX (which is bad considering that taking in new blood is
crucial for both userbase and developer contribution reasons); and
the final nail in the coffin is the fact that the much easier and
more recent Alpha Strike ruleset isn't included at all by default,
to my knowledge. You might be able to configure something like that
using plugins, but we're already putting the cart before the horse
at that point.


TO BE FAIR

The project started in 2000. It's 21 years old in some places, if
only in logic and not literal syntax. It's written in Java. And
it's been a community effort by dozens of talented people over the
decades. The fact that it's alive at all is impressive, but so's
the fact that the franchise whose boardgame it emulates even has a
fanbase still. BT fans aren't quitters, certainly. And this is all
considering the fact that trying to automate, obfuscate and
de-FUBAR the mountain of minutiae that Battletech's rulesets and
technical data (on a per unit and per variant basis no less) is a
monumental task. I can hardly think of a video-game adaptation of a
more complex board game that does a better job, in proportion to
the complexity of the physical source material and gameplay flow.

So considering it was made in the waning days of Win98 (because
Windows ME doesn't exist and you can't convince me otherwise) it's
expected that it's ugly as most things were back then. It was
written in Java (yea verily, begone foul JVM!), which for all its'
foibles makes a very easy cross-platform distributable program.
It's solidly a relic of an earlier time of online gaming, where
dial-up connections were still common. That's a given. And it is
what it is. I won't waste time bemoaning those facts that are
immutable in this context. I'm only stating them to set the stage.

## I'm left at an interesting impasse. There is no other way I
could ever play Battletech with friends from all over the world in
such a great degree of fidelity. Megamek serves its' purpose very
well for what it is: a highly detailed computerization of a niche
franchise that has a fanbase spanning generations. However, it also
stands out to me as one of the prime examples of a FOSS project
being understandably opaque to newcomers. It's hard to use at
first, hard to look at always, and sometimes hard to find other
people to play it with. Those three things will nail the coffin of
any fledgling game shut. That concept is true regardless of genre,
art style, or UI/UX.

So, I leave it to you to put the pieces together here. I can choose
to play with average people that A) aren't technically inclined, B)
don't have a heart that beats for free-as-in-freedom, and C) want
something that "just works," or confine myself to a much smaller
content base with an even smaller playerbase to share it with.

Call me the gaming nerd Whore of Babylon, but I don't see the point
in beating my head against the wall overtly trying to make my hobby
free and open if there isn't a platform for those newcomers to even
stand on and explore once (or if ever) they choose to get involved
in FOSS gaming of their own volition.


AT THE END OF THE DAY,

I encourage people who play video games which are also interested
in free and open source software to consider the prospect of
inviting others into this world of free-as-in-freedom/beer/whatever
with a grain of salt. Until we make it easier to onboard new people
from all walks of life, significant adoption of FOSS systems or the
games that run on them will not be seen. And therefore we will not
see the requisite uptick in talent, contribution and playerbase
that will consequently drive a growth in production quality,
variety, and competitiveness in the market space of people's free
time.


                      - - -[ CONTACT ME ]- - - 

           external email: wholesomedonut at tuta dot io
           mastodon: at wholesomedonut at fosstodon dot org
           matrix: at wholesomedonut colon matrix dot org


- - - ------------------------------------------------------- - - -
 - - - - - --------------------------------------------- - - - - - 
