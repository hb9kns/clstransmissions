``` 
 - - - - - --------------------------------------------- - - - - - 
                       C I R C U M L U N A R
                     T R A N S M I S S I O N S
- - - ------------------------------------------------------- - - -
Issue One                                                  May 2021
```

# Ask Jone

-- Do you have a problem? Send your questions to joneworlds@mailbox.org

W. asks:
> "I have a strained relationship with my sister. What are some things I can do to help repair it?"

I remember my dad and I went through this patch where we were both pretty pissed off at each other, not that I remember why at this point. But sharing is caring, right? So to keep things going, I'm still dropping off cans of Chunk'n'Dunk soup for him, because it's all he ever feels like eating anymore. Although that stuff is just not very nutritious. But because I'm mad at him, I don't bother to bring his favorite, the meatball-carrot stew one. Instead, I bring him can after can of turkey-a-la-king. You ever try that one from Chunk'n'Dunk? Not great. And I guess dad's thinking along the same lines as me, because every time I come by he leaves me another pair of snow tires he found. But I'm not selling tires any more by then, so I don't want them. And he knows that, but he keeps on getting tires anyhow.

And so we go on like this for weeks: I come by and put like 5 more cans of turkey-a-la-king on his kitchen counter, and he stacks a few more snow tires outside his door for me. And all this stuff is piling higher and higher and higher, and we're gradually getting madder and madder at each other. Until one day I go over there with another box of cans, and he starts screaming and yelling about how he hates turkey-a-la-king. And he gets so mad he accidentally knocks over the stack of cans. And some falls on his head and he falls over, and the rest of the cans come crashing down on him. Pretty bad scene. So I go to help him up, but I slip and trip on a can on the floor, and hit the ground hard too. And if you've ever fallen on a pile of Chunk'n'Dunk turkey-a-la-king cans, you know that hurts pretty bad.

So we're both groaning on the ground, and then there's this weird sound by the front door. When I get out there to check it I find that co-incidentally the big pile of snow tires out there has also fallen over, and it's crushed some unlucky little gnome who must have been milling around by there. Those things are so bad at staying alive, I just can't believe it sometimes.

Anyways, if you're looking to patch things up with your sister, I would say maybe don't do it with canned soup, or snow tires. It sure didn't help us any. It just wasn't a good result for anyone, especially for that gnome. Or if you have to go with soup, at least stick with the meatball-carrot one. It's actually not bad.

Thanks for writing in. I hope that helps.

-JONE

```
 ________________________
|\                      /|
| \     joneworlds     / |
|  \        @         /  |
|   \   mailbox.org  /   |
|    \______________/    |
|      /         \       |
|     /           \      |
|____/_____________\_____|

- - - ------------------------------------------------------- - - -
```
=> sloum_circumlunar-mixtape-001.gmi << [ii] sloum's covid-year playlist
=> tfurrows_granular-ideologue.gmi >> [I] Manifesto of a Granular Ideologue - tfurrows
=> index.gmi Circumlunar Transmisisons - Issue 1
```
 - - - - - --------------------------------------------- - - - - - 
```
