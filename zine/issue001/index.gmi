```
       ⠤⠤ ⠠⠤⠤⠤⠄⠠⠤⠤    ⠤⠤ ⠠⠤ ⠤⠄⠠⠤ ⠤⠄ ⠤⠄  ⠠⠤ ⠤⠄⠠⠤ ⠤⠄ ⠠⠤⠄ ⠠⠤⠤        
     ⠰⠿⠉⠈⠁⠈⠉⠿⠏⠁⠸⠿⠈⠽⠇⠰⠿⠋⠈⠁⠸⠿ ⠿⠇⠸⠟⠦⠿⠇ ⠿⠇  ⠸⠿ ⠿⠇⠸⠟⠧⠻⠇ ⠾⠛⠿ ⠸⠿⠈⠽⠇      
     ⠸⠿⠄⠠   ⠿⠇ ⠸⠿⠙⠿⠄⠘⠿⠄  ⠸⠿ ⠿⠇⠸⠇⠛⠸⠇ ⠿⠇  ⠸⠿ ⠿⠇⠸⠿⠸⠾⠇⠸⠿⠶⠿⠇⠸⠿⠙⠿       
      ⠈⠉⠉⠁⠈⠉⠉⠉⠁⠈⠉ ⠈⠉ ⠈⠉⠉⠁ ⠉⠉⠉ ⠈⠁ ⠈⠁ ⠉⠉⠉⠁ ⠈⠉⠉ ⠈⠉ ⠉⠁⠉⠁ ⠈⠉⠈⠉ ⠈⠉      

           [[ the offical zine of circumlunar.space ]]

⠈⠉⠿⠋⠉⠸⠿⠉⠻⠇ ⠸⠟⠷ ⠸⠟⠧⠻⠇⠰⠿⠍⠙⠁⠸⠿⠦⠿⠇⠈⠉⠿⠋⠁⠰⠿⠍⠙⠁⠰⠿⠍⠙⠁⠈⠉⠿⠋⠁⠰⠿⠉⠿⠆⠸⠿⠆⠿⠇⠰⠿⠍⠙⠁ 
  ⠿⠅ ⠸⠿⠻⠿⠁⠠⠿⠶⠿⠇⠸⠿⠹⠼⠇ ⠉⠙⠿⠆⠸⠇⠿⠸⠇  ⠿⠅  ⠈⠙⠿⠧ ⠈⠙⠿⠇  ⠿⠅ ⠸⠿ ⠼⠏⠸⠿⠹⠼⠇ ⠈⠙⠿⠇ 
  ⠛⠂ ⠘⠛ ⠙⠓⠚⠃ ⠘⠛⠘⠛ ⠛⠃⠈⠛⠛⠙⠁⠘⠃ ⠘⠃⠘⠛⠛⠛⠃⠈⠙⠛⠋⠁⠈⠙⠛⠋⠁⠘⠛⠛⠛⠃ ⠙⠛⠋ ⠘⠛ ⠛⠃⠈⠙⠛⠋⠁ 
```

# Circumlunar Transmissions - Issue 1, April 2021

## Editions
=> spreads/ Printer Spreads
=> broadsheet-001.txt Plaintext Broadsheet

## Preface

=> preface.gmi [i] Editor's Introduction

## Columns

### The Circumlunar Mixtape

The Circumlunar Mixtape is a playlist of ten songs curated for every issue of the zine by a sundog of circumlunar.space. For this inaugural issue, sloum hand-picks an ecclectic mix of tracks that provided quality ear candy for them during the first year of the pandemic.
=> sloum_circumlunar-mixtape-001.gmi [ii] sloum's covid-year playlist

### Ask Jone

Having family troubles? Lost in life? Stumped on a crossword puzzle? Need a good waffle recipe?
=> ask-jone-001.gmi [iii] Ask Jone!

## Features

=> tfurrows_granular-ideologue.gmi [I] Manifesto of a Granular Ideologue - tfurrows

=> solderpunk_whats-the-deal-with-leapseconds.gmi [II] What's the Deal with Leap Seconds? - solderpunk

=> durtal_hearth-of-the-matter.gmi [III] The Hearth of the Matter - durtal

=> wholesomedonut_why-i-still-game-proprietary.gmi [IV] Why I Still Game Proprietary - wholesomedonut

```
- - - ------------------------------------------------------- - - -
```
=> ../ Circumlunar Transmisisons - Home
```
 - - - - - --------------------------------------------- - - - - - 
```
