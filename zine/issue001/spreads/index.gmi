``` 
 - - - - - --------------------------------------------- - - - - - 
                       C I R C U M L U N A R
                     T R A N S M I S S I O N S
- - - ------------------------------------------------------- - - -
Issue One                                                  May 2021
```

# Printer Spreads

For this issue, a print-ready A5 Booklet is available:

=> ct-001-A5booklet_shortside.pdf A5 Booklet (short-side printing)
=> ct-001-A5booklet_longside.pdf A5 Booklet (long-side printing)

For standard home printers, the short-side version should be appropriate. If you are printing using an office printer/copier, then you should likely use the long-side version.

All that is needed to print and bind this booklet (besides a printer) is:

* 8 Sheets of standard weight paper (~60-80g)
* A stapler 
* A piece of corrugated cardboard or something similar

After printing, ensure that the pages are properly collated, then simply fold them in half. Crease it as hard as possible. Reopen the booklet and lay it flat over a piece of cardboard on a hard surface (the cover page should be facing up). Staple two or three times along the "spine." The ends of the staples should penetrate the cardboard. Turn the booklet over, and using a blunt object, bend the ends of the staples inward along the spine. And that's it!

The source LaTeX and PDF files are also available:

=> ct-001.latex ct-001.latex
=> ct-001.pdf ct-001.pdf

```
- - - ------------------------------------------------------- - - -
```
=> ../ Circumlunar Transmisisons - Issue One
```
 - - - - - --------------------------------------------- - - - - - 
```


