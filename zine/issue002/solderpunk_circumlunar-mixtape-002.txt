# Circumlunar Mixtape

Circumlunar Mixtape is an ongoing series for Circumlunar Transmissions where one
user per issue shares 10 tracks they have been listening to. Y'all have all
kinds of ways to stream or otherwise find and listen to music, so tracks are
just listed and it is on the reader to locate them. As a courtesy, when a
weblink to streaming is available playlist curators may choose to supply it.

## Solderpunk's space-psych-jazz-techno salad

Here's a somewhat ecclectic collection of tracks I have been digging lately.
They are all "new to me", in the sense that I hadn't even heard of any of these
artists a year ago.  Maybe a year and a half in a few cases.  There is a healthy
serving of stuff from the space- / psych- / krautrock category, but to keep
things interesting there are also some electronic tracks in there and even a few
jazz pieces.  I hope you enjoy!

.------------------------.
| CT002                  |
|     __  ______  __     |
|    /. \|\.....|/  \    |
|    \__/|/_____|\__/    |
|             solderpunk |
|    ________________    |
|___/_._o________o_._\___|

Format: [track name], by [artist name], from [album name]

[1]  Lemon and Ginger, by Susumu Yokota, from Cat, Mouse and Me
[2]  E2-E4, by Manuel Göttsching (see footnote 1)
[3]  De Partida no Teleporto, by Samuel Cadima, from Outros Horizontes
[4]  You Play For Us Today, by Agitation Free, from Malesch (see footnote 2)
[5]  Alabamian Horologists, by Lamagaia, from Garage Space Vol. 1
[6]  We Where On The Moon Before You Where On The Moon, by Space Debris, from Krautrocksessions 1994 - 2001
[7]  Cathedral, by Mt Mountain, from Golden Rise
[8]  Black Venom, by the Budos Band, from The Budos Band III
[9]  Kofi by Donald Byrd, from Kofi
[10] Switch Blade, by Duke Ellington, Charlie Mingus and Max Roach, from Money Jungle

Links:

[1] https://www.youtube.com/watch?v=hYiG57ZKAxY&t=1745s
[2] https://www.youtube.com/watch?v=ys0HyevZpQg
[3] https://samuelcadima.bandcamp.com/track/de-partida-no-teleporto
[4] https://www.youtube.com/watch?v=6qzHt1X7hDY
[5] https://lamagaia.bandcamp.com/track/alabamian-horologists
[6] https://spacedebris.bandcamp.com/track/we-where-on-the-moon-before-you-where-on-the-moon
[7] https://mtmountain.bandcamp.com/track/cathedral-2
[8] https://thebudosband.bandcamp.com/track/black-venom
[9] https://www.youtube.com/watch?v=skGQV6N-KSQ
[10] https://www.youtube.com/watch?v=o5D8k5I8LkA

1. Alright, technically E2-E4 is an album and not a track, but for all intents
and purposes it *is* just one long track without meaningful transitions so it
makes no sense to me to highlight some arbitrary subsection.  To be honest, my
music listening is heavily album-centric these days so you should consider
yourselves lucky I was able to pick out tracks at all!

2. This is my most recently discovered track in the playlist.  I am enthralled
with the entire album.
