#!/usr/bin/python3

"""
populate.py

This script should be run from within `bin/` with a single argument, which
should correspond to a director in `src/`, e.g. `./populate.py issue001`.
It will then create a corresponding directory in `zine` and populate it
with:

* Exact copies of any .gmi files found in e.g. `src/issue001/texts`
* Roughly Gopher-friendly .txt files generated from each .gmi file
  (wrapped to 80 characters)
* A template index.gmi file containing links to all the .gmi files
* A template gophermap file containing links to all the .txt files

The idea is *not* that after adding all the submissions to `src/*/texts` you
run this script and hey-presto that zine issue is ready.  This is just to
automate the drudge work so you can focus on doing the stuff that isn't so
easily automated.
"""

import glob
import os
import os.path
import shutil
import sys
import textwrap

def populate_issue(issue):
    # Make sure issue exists
    issue_src_path = os.path.join("../src", issue)
    if not os.path.exists(issue_src_path):
        print("Could not find issue {}!".format(issue_src_path))
        return

    # Make sure issue contains articles
    articles = glob.glob(os.path.join(issue_src_path, "texts", "*.gmi"))
    if not articles:
        print("No articles found in {}!".format(issue_src_path))
        return

    # Create destination directory
    issue_dst_path = os.path.join("..", "zine", issue)
    if not os.path.exists(issue_dst_path):
        os.mkdir(issue_dst_path)

    # Copy .gmi files
    dest_articles = []
    for article in articles:
        article_filename = os.path.split(article)[1]
        print("Copying article {}...".format(article_filename))
        dest_path = os.path.join(issue_dst_path, article_filename)
        shutil.copy(article, dest_path)
        dest_articles.append(dest_path)

    # Create index
    index_filename = os.path.join(issue_dst_path, "index.gmi")
    create_index(index_filename, issue, dest_articles)

    # Translate .gmi articles to .txt for Gopher
    for article in dest_articles:
        article_filename = os.path.split(article)[1]
        txt_path = os.path.splitext(article)[0] + ".txt"
        txt_filename = os.path.split(txt_path)[1]
        print("Translating article {} to {}...".format(article_filename, txt_filename))
        gemini_to_gopher(article, txt_path)

    # TODO: Translate index to gophermap

def create_index(index_file, issue, article_list):
    articles = [os.path.split(x)[1] for x in article_list]
    fp = open(index_file, "w")
    # Title
    fp.write("# Circumlunar Transmissions - {}\n\n".format(issue))

    # Preface
    if "preface.gmi" in articles:
        fp.write("## Preface\n\n")
        fp.write("=> preface.gmi Editor's introduction\n\n")
    
    # Features
    fp.write("## Features\n\n")
    for article in article_list:
        article_filename = os.path.split(article)[1]
        if article_filename == "preface.gmi":
            continue
        with open(article, "r") as fp_art:
            first_line = fp_art.readline()
        if first_line.startswith("#"):
            title = first_line[1:].strip()
        else:
            title = article_filename
        fp.write("=> {} {}\n".format(article_filename, title))

    fp.close()

def gemini_to_gopher(article, txt_path):
    fp_in = open(article, "r")
    fp_out = open(txt_path, "w")
    verbatim = False
    for line in fp_in:
        if line.startswith("```"):
            verbatim = not verbatim
        elif verbatim:
            fp_out.write(line)
        elif line.startswith("=> "):
            fp_out.write(line)
        else:
            fp_out.write(textwrap.fill(line, 80)+"\n")
    fp_in.close()
    fp_out.close()

issues = sys.argv[1:]

for issue in issues:
    populate_issue(issue)

